package com.lg.mykeystore;

public interface RSAHandler {
    String RSAEncrypt();

    String RSADecrypt();
}

package com.lg.mykeystore;

import android.app.Activity;

import com.lg.fingerprintlibrary.FingerCallback;

public interface AESHandler {
   // byte[] AESEncrypt(String alias,  String textToEncrypt) throws Exception;

    //String  AESDecrypt(String alias,  byte[] encryptedData,  byte[] encryptionIv) throws Exception;

    void FingerAESEncrypt(Activity activity,String alias, String textToEncrypt, KeystoreFingerCallback Callback) throws Exception;

    void  FingerAESDecrypt(Activity activity, String alias, byte[] encryptedData, byte[] encryptionIv, KeystoreFingerCallback Callback) throws Exception;
}

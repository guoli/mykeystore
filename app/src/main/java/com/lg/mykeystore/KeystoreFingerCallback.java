package com.lg.mykeystore;

public interface KeystoreFingerCallback {

        void onFingerKeyStoreResponse(byte[] response);

    }
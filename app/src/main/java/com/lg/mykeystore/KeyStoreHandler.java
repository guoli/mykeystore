package com.lg.mykeystore;

import android.app.Activity;
import android.hardware.biometrics.BiometricPrompt;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyInfo;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;

import com.lg.fingerprintlibrary.FingerCallback;
import com.lg.fingerprintlibrary.FingerprintAuthenticationDialogFragment;
import com.lg.fingerprintlibrary.FingerprintUiHelper;

import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class KeyStoreHandler implements AESHandler ,FingerCallback{
        String TAG= KeyStoreHandler.class.getSimpleName();

    private static final String AESCBCPK_TRANSFORMATION = KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC+ "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7;
    private static final String TRANSFORMATION = "AES/GCM/NoPadding";
    private static final String ANDROID_KEY_STORE = "AndroidKeyStore";

    private byte[] encryption;
    private byte[] iv;
    String mTextToEncrypt;

    byte[]mEncryptedData;

    boolean isEncrypt =false;

    KeystoreFingerCallback mKeystoreFingerCallback;

    @RequiresApi(api = Build.VERSION_CODES.M)
    private SecretKey getEncryptSecretKey( String alias,boolean isAuthenticationRequired) throws Exception {

         KeyGenerator keyGenerator = KeyGenerator
                .getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE);

        keyGenerator.init(new KeyGenParameterSpec.Builder(alias,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .setUserAuthenticationRequired(isAuthenticationRequired)
                .build());

        return keyGenerator.generateKey();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private SecretKey getDecryptSecretKey( String alias,boolean isAuthenticationRequired) throws Exception {

        KeyGenerator keyGenerator = KeyGenerator
                .getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE);

        keyGenerator.init(new KeyGenParameterSpec.Builder(alias,
                KeyProperties.PURPOSE_DECRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setUserAuthenticationRequired(isAuthenticationRequired)
                .build());

        return keyGenerator.generateKey();
    }

    byte[] getEncryption() {
        return encryption;
    }

    byte[] getIv() {
        return iv;
    }


//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public byte[] AESEncrypt(String alias,  String textToEncrypt) throws Exception{
//         Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//         SecretKey key = getEncryptSecretKey(alias,false);
//        SecretKeyFactory factory = SecretKeyFactory.getInstance(key.getAlgorithm(), "AndroidKeyStore");
//        KeyInfo keyInfo;
//        try {
//            keyInfo = (KeyInfo) factory.getKeySpec(key, KeyInfo.class);
//            String aliasName=keyInfo.getKeystoreAlias();
//            boolean isInsideSecureHardware=keyInfo.isInsideSecureHardware();
//            Log.d(TAG,"aliasName="+aliasName+"|isInsideSecureHardware="+isInsideSecureHardware);
//        } catch (InvalidKeySpecException e) {
//            // Not an Android KeyStore key.
//            Log.e(TAG,"Exception="+e.getMessage());
//        }
//
//        cipher.init(Cipher.ENCRYPT_MODE, key);
//        iv = cipher.getIV();
//        return (encryption = cipher.doFinal(textToEncrypt.getBytes("UTF-8")));
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//    @Override
//   public String AESDecrypt( String alias,  byte[] encryptedData,  byte[] encryptionIv)
//            throws Exception {
//        KeyStore keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
//        keyStore.load(null);
//         Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//         GCMParameterSpec spec = new GCMParameterSpec(128, encryptionIv);
//        cipher.init(Cipher.DECRYPT_MODE, getDecryptSecretKey(keyStore,alias), spec);
//
//        return new String(cipher.doFinal(encryptedData), "UTF-8");
//    }

    private SecretKey getDecryptSecretKey(KeyStore keyStore, String alias) throws Exception {
        return ((KeyStore.SecretKeyEntry) keyStore.getEntry(alias, null)).getSecretKey();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void FingerAESEncrypt(Activity activity,String alias, String textToEncrypt, KeystoreFingerCallback fingerCallback) throws Exception {
        mKeystoreFingerCallback=fingerCallback;
        isEncrypt =true;
        Cipher cipher = Cipher.getInstance(AESCBCPK_TRANSFORMATION);
        SecretKey key = getEncryptSecretKey(alias,true);
        SecretKeyFactory factory = SecretKeyFactory.getInstance(key.getAlgorithm(), ANDROID_KEY_STORE);
        KeyInfo keyInfo;
        try {
            keyInfo = (KeyInfo) factory.getKeySpec(key, KeyInfo.class);
            String aliasName=keyInfo.getKeystoreAlias();
            boolean isInsideSecureHardware=keyInfo.isInsideSecureHardware();
            Log.d(TAG,"aliasName="+aliasName+"|isInsideSecureHardware="+isInsideSecureHardware);
        } catch (InvalidKeySpecException e) {
            // Not an Android KeyStore key.
            Log.e(TAG,"Exception="+e.getMessage());
        }
        this.mTextToEncrypt = textToEncrypt;
        Log.e(TAG,"mTextToEncrypt="+mTextToEncrypt);

        cipher.init(Cipher.ENCRYPT_MODE, key);
        FingerprintAuthenticationDialogFragment fingerprintAuthenticationDialogFragment =new FingerprintAuthenticationDialogFragment();
        fingerprintAuthenticationDialogFragment.setCryptoObject(cipher);
        fingerprintAuthenticationDialogFragment.setFingerprintCallBack(this);
        fingerprintAuthenticationDialogFragment.show(activity.getFragmentManager(), "fingerdialog");



    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void FingerAESDecrypt(Activity activity,String alias, byte[] encryptedData, byte[] encryptionIv,KeystoreFingerCallback fingerCallback) throws Exception {
        isEncrypt =false;
        mKeystoreFingerCallback=fingerCallback;

        KeyStore keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
        keyStore.load(null);
        SecretKey mSecretKey = (SecretKey) keyStore.getKey(alias, null);

        Cipher cipher = Cipher.getInstance(AESCBCPK_TRANSFORMATION);

        IvParameterSpec ivSpec = new IvParameterSpec(encryptionIv);

        cipher.init(Cipher.DECRYPT_MODE, mSecretKey, ivSpec);
        this.mEncryptedData =encryptedData;
        FingerprintAuthenticationDialogFragment fingerprintAuthenticationDialogFragment =new FingerprintAuthenticationDialogFragment();
        fingerprintAuthenticationDialogFragment.setCryptoObject(cipher);
        fingerprintAuthenticationDialogFragment.setFingerprintCallBack(this);
        fingerprintAuthenticationDialogFragment.show(activity.getFragmentManager(), "fingerdialog");

        //new String(cipher.doFinal(encryptedData), "UTF-8");
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onFingerDialogAuthenticated(FingerprintManager.CryptoObject cryptoObject) {
        Cipher mCipher = cryptoObject.getCipher();
        byte[] encrypt_decrypte;
        try {
            if(isEncrypt){
                this.iv = mCipher.getIV();
                Log.e(TAG,"onFingerDialogAuthenticated|mTextToEncrypt="+mTextToEncrypt);

                this.encryption = mCipher.doFinal(mTextToEncrypt.getBytes("UTF-8"));
                encrypt_decrypte=this.encryption;
               // this.encryption=encrypt_decrypte;
            }else {
                encrypt_decrypte= mCipher.doFinal(this.mEncryptedData);
            }

            mKeystoreFingerCallback.onFingerKeyStoreResponse(encrypt_decrypte);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Exception="+e.getMessage());

        }
    }

    @Override
    public void onFingerDialogError() {

    }


}

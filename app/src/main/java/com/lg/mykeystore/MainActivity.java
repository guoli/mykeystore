package com.lg.mykeystore;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;

public class MainActivity extends AppCompatActivity {
    String TAG=MainActivity.class.getSimpleName();
Context context;
    EditText ed_plaintext;
    TextView tv_result;
    KeyStoreHandler keyStoreHandler ;
    EditText ed_alias;
    String aliasName;
    EditText ed_alias2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(getSupportActionBar()!=null){
        getSupportActionBar().hide();
        }
        context=this;
        ed_plaintext=findViewById(R.id.ed_plaintext);
        tv_result = findViewById(R.id.tv_result);
        ed_alias= findViewById(R.id.ed_alias);
        ed_alias2=findViewById(R.id.ed_alias2);
        keyStoreHandler= new KeyStoreHandler();
        aliasName =ed_alias.getText().toString();
        Log.d(TAG,"open");


    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    public static String generateSecureRandomKey() throws Exception{
        String maeskey="";
        try {
            SecureRandom mSecureRandom =new SecureRandom();
            byte[] bytes_key = new byte[256 / Byte.SIZE];
            mSecureRandom.nextBytes(bytes_key);
            maeskey = bytesToHexString(bytes_key);
        } catch (Exception e) {
            e.printStackTrace();
            // ErrorInfo errorInfo = new ErrorInfo(e.getMessage()+"", YESTokenAPIConstant.GENERATE_AESKEY_ERROR);
            //String json = new JSONSerializer().exclude(excludeFiled).deepSerialize(errorInfo);
            //throw new Exception(json);
        }
        return maeskey;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onDecrypt(View view) {

        Log.e(TAG,"onDecrypt = ");
        try {
            aliasName =ed_alias.getText().toString();
           String encodeText= tv_result.getText().toString();
           byte [] encodeContent = Base64.decode(encodeText.getBytes("UTF-8"),Base64.DEFAULT|Base64.NO_WRAP);
            keyStoreHandler.FingerAESDecrypt(this,aliasName,encodeContent,keyStoreHandler.getIv(),new KeystoreFingerCallback(){

                @Override
                public void onFingerKeyStoreResponse(byte[] response) {
                    Log.e(TAG,"onFingerKeyStoreResponse");

                    String DecrypteText = null;
                    try {

                       // Log.e(TAG,"response = "+new String (response,"UTF-8"));

                        //DecrypteText = new String ((Base64.decode(response,Base64.DEFAULT|Base64.NO_WRAP)),"UTF-8");
                        Log.e(TAG,"Base64 response = "+DecrypteText);

                        tv_result.setText(new String (response,"UTF-8"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this,"Exception="+e.getMessage(),Toast.LENGTH_SHORT).show();
            Log.e(TAG,"Exception="+e.getMessage());

        }



    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onEncrypt(View view) {
        Log.d(TAG,"onEncrypt");

        aliasName =ed_alias.getText().toString();
        String paintext = ed_plaintext.getText().toString();
        Log.d(TAG,"AES Encrypt 1:encode data = "+paintext);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            try {
               // byte[] encryptedText = keyStoreHandler.AESEncrypt(aliasName,paintext);
              keyStoreHandler.FingerAESEncrypt(this,aliasName,paintext,new KeystoreFingerCallback(){

                    @Override
                    public void onFingerKeyStoreResponse(byte[] response) {
                        try {
                            tv_result.setText(new String(Base64.encode(response,Base64.DEFAULT|Base64.NO_WRAP), "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG,"Exception="+e.getMessage());
            }
        }else {
            Toast.makeText(this,"SDK_INT="+android.os.Build.VERSION.SDK_INT,Toast.LENGTH_SHORT).show();
        }
    }

//    @RequiresApi(api = Build.VERSION_CODES.M)
//    public void onReDecrypt(View view) {
//        try {
//            aliasName =ed_alias2.getText().toString();
//            //String DecrypteText= keyStoreHandler.AESDecrypt(aliasName,keyStoreHandler.getEncryption(),keyStoreHandler.getIv());
//            keyStoreHandler.FingerAESDecrypt(this,aliasName,keyStoreHandler.getEncryption(),keyStoreHandler.getIv(),new KeystoreFingerCallback(){
//
//                @Override
//                public void onFingerKeyStoreResponse(byte[] response) {
//                    String DecrypteText = null;
//                    try {
//                        DecrypteText = new String (response,"UTF-8");
//                        tv_result.setText(DecrypteText);
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e(TAG,"Exception="+e.getMessage());
//
//        }
//    }
}



# KeyStore By FingerPrint

## 一. generateSecureRandomKey

```
    
    public static String generateSecureRandomKey() throws Exception{
        String maeskey="";
        try {
            SecureRandom mSecureRandom =new SecureRandom();
            byte[] bytes_key = new byte[256 / Byte.SIZE];
            mSecureRandom.nextBytes(bytes_key);
            maeskey = bytesToHexString(bytes_key);
        } catch (Exception e) {
            e.printStackTrace();
            // ErrorInfo errorInfo = new ErrorInfo(e.getMessage()+"", YESTokenAPIConstant.GENERATE_AESKEY_ERROR);
            //String json = new JSONSerializer().exclude(excludeFiled).deepSerialize(errorInfo);
            //throw new Exception(json);
        }
        return maeskey;
    }
```

## 二. Api直接调用

### 1.初始化keyStoreHandler

```
keyStoreHandler= new KeyStoreHandler();

```

### 2.指纹加密 FingerAESEncrypt
```
      keyStoreHandler.FingerAESEncrypt(this,aliasName,paintext,new KeystoreFingerCallback(){

                    @Override
                    public void onFingerKeyStoreResponse(byte[] response) {
                        try {
                            tv_result.setText(new String(Base64.encode(response,Base64.DEFAULT|Base64.NO_WRAP), "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }
                });
```

### 3.指纹解密

```
 String encodeText= tv_result.getText().toString();
    byte [] encodeContent = Base64.decode(encodeText.getBytes("UTF-8"),Base64.DEFAULT|Base64.NO_WRAP);
     keyStoreHandler.FingerAESDecrypt(this,aliasName,encodeContent,keyStoreHandler.getIv(),new KeystoreFingerCallback(){

                @Override
                public void onFingerKeyStoreResponse(byte[] response) {
                    try {
                        tv_result.setText(new String (response,"UTF-8"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
```


-----


## 三. 指纹加密详细细节


###  1. 初始化Cipher

```
    //create SecretKey
    private SecretKey getEncryptSecretKey( String alias,boolean isAuthenticationRequired) throws Exception {

         KeyGenerator keyGenerator = KeyGenerator
                .getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE);

        keyGenerator.init(new KeyGenParameterSpec.Builder(alias,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .setUserAuthenticationRequired(isAuthenticationRequired)
                .build());

        return keyGenerator.generateKey();
    }

    //cipher.init
Cipher cipher = Cipher.getInstance(AESCBCPK_TRANSFORMATION);
        SecretKey key = getEncryptSecretKey(alias,true);
        SecretKeyFactory factory = SecretKeyFactory.getInstance(key.getAlgorithm(), ANDROID_KEY_STORE);
        KeyInfo keyInfo;
        try {
            keyInfo = (KeyInfo) factory.getKeySpec(key, KeyInfo.class);
            String aliasName=keyInfo.getKeystoreAlias();
            boolean isInsideSecureHardware=keyInfo.isInsideSecureHardware();
        } catch (InvalidKeySpecException e) {
            // Not an Android KeyStore key.
        }

        cipher.init(Cipher.ENCRYPT_MODE, key);
```

### 2. 设置指纹加密对象和开启指纹验证界面 FingerprintManager.CryptoObject


```
FingerprintAuthenticationDialogFragment fingerprintAuthenticationDialogFragment =new FingerprintAuthenticationDialogFragment();
        fingerprintAuthenticationDialogFragment.setCryptoObject(cipher);
        fingerprintAuthenticationDialogFragment.setFingerprintCallBack(this);
        fingerprintAuthenticationDialogFragment.show(activity.getFragmentManager(), "fingerdialog");

```


### 3. 验证回调进行加密数据和保存IV，返回加密数据。

```
  @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onFingerDialogAuthenticated(FingerprintManager.CryptoObject cryptoObject) {
        Cipher mCipher = cryptoObject.getCipher();
        byte[] encrypt_decrypte;
        try {
                this.iv = mCipher.getIV();

                //mTextToEncrypt 加密数据
                this.encryption = mCipher.doFinal(mTextToEncrypt.getBytes("UTF-8"));
          

            mKeystoreFingerCallback.onFingerKeyStoreResponse(encrypt_decrypte);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
```

### 4.解密(初始化cipher和调用指纹UI)

```
 KeyStore keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
        keyStore.load(null);
        SecretKey mSecretKey = (SecretKey) keyStore.getKey(alias, null);

        Cipher cipher = Cipher.getInstance(AESCBCPK_TRANSFORMATION);

        IvParameterSpec ivSpec = new IvParameterSpec(encryptionIv);

        cipher.init(Cipher.DECRYPT_MODE, mSecretKey, ivSpec);
        this.mEncryptedData =encryptedData;
        FingerprintAuthenticationDialogFragment fingerprintAuthenticationDialogFragment =new FingerprintAuthenticationDialogFragment();
        fingerprintAuthenticationDialogFragment.setCryptoObject(cipher);
        fingerprintAuthenticationDialogFragment.setFingerprintCallBack(this);
        fingerprintAuthenticationDialogFragment.show(activity.getFragmentManager(), "fingerdialog");

```


### 5.验证后回调

```
  public void onFingerDialogAuthenticated(FingerprintManager.CryptoObject cryptoObject) {
        Cipher mCipher = cryptoObject.getCipher();
        byte[] encrypt_decrypte;
        try {
                //mEncryptedData 密文
                encrypt_decrypte= mCipher.doFinal(this.mEncryptedData);
  

            mKeystoreFingerCallback.onFingerKeyStoreResponse(encrypt_decrypte);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Exception="+e.getMessage());

        }
    }
```

package com.lg.fingerprintlibrary;

import android.hardware.biometrics.BiometricPrompt;
import android.hardware.fingerprint.FingerprintManager;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;

import javax.crypto.Cipher;

public interface FingerCallback {

         void onFingerDialogAuthenticated(FingerprintManager.CryptoObject cryptoObject);

        void onFingerDialogError();
        
    }